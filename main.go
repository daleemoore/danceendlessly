// Originally from: https://stackoverflow.com/questions/68628246/sql-query-hangs-or-crashes
//
package main

import (
	"log"
	"os"
	//"testing"
	"database/sql"
	_ "modernc.org/sqlite"
	//_ "gitlab.com/cznic/sqlite"
)

func main() {
	//func Test_Bug3(t *testing.T) {
	log.Println("danceEndlessly begins")
	failOrNot := true // Make this false to avoid the -journal endless dance.

	DBPath := "test.db"
	os.Remove(DBPath)
	DB, err := sql.Open("sqlite", DBPath)
	if err != nil {
		log.Fatalf("%s: %v", DBPath, err)
	}
	if _, err := DB.Exec(`CREATE TABLE IF NOT EXISTS verdictcache (sha1 text);`); err != nil {
		log.Fatalf("%s: %v", DBPath, err)
	}
	_, err = DB.Exec("INSERT OR REPLACE INTO verdictcache (sha1) VALUES ($1)", "a")
	if err != nil {
		log.Fatalf("%s: %v", DBPath, err)
	}
	res, err := DB.Query("SELECT * FROM verdictcache WHERE sha1=$1", "a")
	if err != nil {
		log.Fatalf("%s: %v", DBPath, err)
	}

	// TODO, URGENT failOrNot == true will dance endlessly around creating
	//		then deleting test.db-journal.

	if failOrNot {
		log.Println("failOrNot do NOT CLOSE:", failOrNot)
		//res.Close() 	// No .Close, FAIL
	} else {
		log.Println("failOrNot CLOSE:", failOrNot)
		res.Close() // .Close, NOT FAIL
	}

	log.Println(".Exec will appear to hang if filaOrNot == true:", failOrNot)
	log.Println("Look at your source code folder and you will see test.db-journal being created then deleted!")
	_, err = DB.Exec("INSERT OR REPLACE INTO verdictcache (sha1) VALUES ($1)", "b")
	if err != nil {
		log.Fatalf("%s: %v", DBPath, err)
	}
	log.Println("danceEndlessly ends")
}
